﻿using Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfCalculater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        //<Setter Property="Visibility" Value="Hidden" />

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private double _P;
        public double P
        {
            get { return _P; }
            set
            {
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _P = value;
                if (_P > -1)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res1)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res2)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res5)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res6)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res7)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res9)));
            }
        }
        private double _U;
        public double U
        {
            get { return _U; }
            set
            {
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _U = value;
                if (_U > -1)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res1)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res3)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res8)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res9)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res10)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res12)));
            }
        }
        private double _R;
        public double R
        {
            get { return _R; }
            set
            {
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _R = value;
                if (_R > -1)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res2)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res3)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res4)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res6)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res11)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res12)));
            }
        }

        private double _A;
        public double A
        {
            get { return _A; }
            set
            {
                if (value <= -1)
                {
                    MessageBox.Show("Значение должно быть положительным");
                    return;
                }
                _A = value;
                if (_A > -1)
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res4)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res5)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res7)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res8)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res10)));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Res11)));

            }
        }

        public double Res1
        {
        get {
                double res = new CalculateI(P, U, R).CalculateIByPAndU();
                Math.Round(res, 5);
                return res;
            }
        }
        public double Res2
        {
            get {
                double res = new CalculateI(P, U, R).CalculateIByPAndR();
                Math.Round(res, 5);
                return res;
            }
        }
        public double Res3
        {
            get {
                double res = new CalculateI(P, U, R).CalculateIByUAndR();
                Math.Round(res, 5);
                return res;
            }
        }
        public double Res4
        {
            get
            {
                double res = new CalculateU(P, A, R).CalculateUByAAndR();
                Math.Round(res, 5);
                return res;
            }
        }

        public double Res5
        {
            get
            {
                double res = new CalculateU(P, A, R).CalculateUByAAndP();
                Math.Round(res, 5);
                return res;
            }
        }

        public double Res6
        {
            get
            {
                double res = new CalculateU(P, A, R).CalculateUByPAndR();
                Math.Round(res, 5);
                return res;
            }
        }

        public double Res7
        {
            get
            {
                double res = new CalculateR(P, A, U).CalculateRByPAndA();
                Math.Round(res, 5);
                return res;
            }
        }

        public double Res8
        {
            get
            {
                double res = new CalculateR(P, U, A).CalculateRByUAndA();
                Math.Round(res, 5);
                return res;
            }
        }

        public double Res9
        {
            get
            {
                double res = new CalculateR(P, U, A).CalculateRByUAndP();
                Math.Round(res, 5);
                return res;
            }
        }

        public double Res10
        {
            get
            {
                double res = new CalculateP(U, A, R).CalculatePByUAndA();
                Math.Round(res, 5);
                return res;
            }
        }

        public double Res11
        {
            get
            {
                double res = new CalculateP(A, R, U).CalculatePByAAndR();
                Math.Round(res, 5);
                return res;
            }
        }

        public double Res12
        {
            get
            {
                double res = new CalculateP(U, R, A).CalculatePByUAndR();
                Math.Round(res, 5);
                return res;
            }
        }

    }
}
