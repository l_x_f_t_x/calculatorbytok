﻿using System;

namespace Library
{
    //Расчет тока
    public class CalculateI
    {
        private double P;
        private double U;
        private double R;

        public CalculateI(double P = 0, double U = 0, double R = 0)
        {
            this.P = P;
            this.U = U;
            this.R = R;
        }

        //Расчет тока по мощности и напряжению(Постоянный ток)
        public double CalculateIByPAndU()
        {
            return (this.P / this.U);
        }

        //Расчет тока по мощности и сопротивлению
        public double CalculateIByPAndR()
        {
            return Math.Sqrt(this.P / this.R);
        }

        //Расчет тока по напряжению и сопротивлению
        public double CalculateIByUAndR()
        {
            return (this.U / this.R);
        }

    }

    //Расчет мощности
    public class CalculateP
    {
        private double A;
        private double U;
        private double R;

        public CalculateP(double A = 0, double U = 0, double R = 0)
        {
            this.A = A;
            this.U = U;
            this.R = R;
        }

        //Расчет мощности по напряжению и силе тока
        public double CalculatePByUAndA()
        {
            return (this.A * this.U);
        }

        //Расчет мощности по силе тока и сопротилению
        public double CalculatePByAAndR()
        {
            return (Math.Pow(this.A, 2) * this.R);
        }

        //Расчет мощности по напряжению и сопротилению
        public double CalculatePByUAndR()
        {
            return (Math.Pow(this.U, 2) / this.R);
        }
    }

    //Расчет сопротивления
    public class CalculateR
    {
        private double P;
        private double U;
        private double A;
        public CalculateR(double P = 0, double U = 0, double A = 0)
        {
            this.P = P;
            this.U = U;
            this.A = A;
        }

        //Расчет сопротивления по мощности и силе тока
        public double CalculateRByPAndA()
        {
            return (this.P / Math.Pow(this.A, 2));
        }

        //Расчет сопротивления по напряжению и силе тока
        public double CalculateRByUAndA()
        {
            return (this.U / this.A);
        }

        //Расчет сопротивления по напряжению и мощности
        public double CalculateRByUAndP()
        {
            return ((Math.Pow(this.U, 2)) / this.P);
        }
    }

    //Расчет напряжения
    public class CalculateU
    {
        private double P;
        private double A;
        private double R;

        public CalculateU(double P = 0, double A = 0, double R = 0)
        {
            this.P = P;
            this.A = A;
            this.R = R;
        }

        //Расчет напряжения по силе тока и сопротивлению
        public double CalculateUByAAndR()
        {
            return (this.A * this.R);
        }

        //Расчет напряжения по силе тока и мощности
        public double CalculateUByAAndP()
        {
            return (this.P / this.A);
        }

        //Расчет напряжения по мощности и сопротивлению
        public double CalculateUByPAndR()
        {
            return (Math.Sqrt(this.P * this.R));
        }
    }
}