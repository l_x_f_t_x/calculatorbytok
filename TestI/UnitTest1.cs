using NUnit.Framework;
using System;

namespace Test
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            const double p = 1000;
            const double u = 220;
            const double expected = 4.5454545454545459;

            var result = new Library.CalculateI(P: p, U: u).CalculateIByPAndU();

            Assert.That(result, Is.EqualTo(expected));
        }



        public void Test2()
        {
            const double p = 1000;
            const double r = 220;
            const double expected = 2.132007164;

            var result = new Library.CalculateI(P: p, R: r).CalculateIByPAndR();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test3()
        {
            const double u = 1000;
            const double r = 220;
            const double expected = 22;

            var result = new Library.CalculateI(U: u, R: r).CalculateIByUAndR();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test4()
        {
            const double a = 2860;
            const double u = 13;
            const double expected = 37180;

            var result = new Library.CalculateP(A: a, U: u).CalculatePByUAndA();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test5()
        {
            const double a = 4.545454545;
            const double r = 220;
            const double expected = 4545.454545;

            var result = new Library.CalculateP(A: a, R: r).CalculatePByAAndR();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test6()
        {
            const double u = 4.545454545;
            const double r = 220;
            const double expected = 0.09391435;

            var result = new Library.CalculateP(U: u, R: r).CalculatePByUAndR();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test7()
        {
            const double p = 13;
            const double a = 220;
            const double expected = 0.000268595;

            var result = new Library.CalculateR(P: p, A: a).CalculateRByPAndA();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test8()
        {
            const double u = 13;
            const double a = 220;
            const double expected = 0.059090909;

            var result = new Library.CalculateR(U: u, A: a).CalculateRByUAndA();

            Assert.That(result, Is.EqualTo(expected));
        }
        public void Test9()
        {
            const double u = 13;
            const double p = 220;
            const double expected = 0.768181818;

            var result = new Library.CalculateR(U: u, P: p).CalculateRByUAndP();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test10()
        {
            const double A = 10;
            const double R = 2;
            const double expected = 20;

            var result = new Library.CalculateU(A: A, R: R).CalculateUByAAndR();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test11()
        {
            const double A = 10;
            const double P = 2;
            const double expected = 5;

            var result = new Library.CalculateU(A: A, P: P).CalculateUByAAndP();

            Assert.That(result, Is.EqualTo(expected));
        }

        public void Test12()
        {
            const double P = 50;
            const double R = 2;
            const double expected = 5;

            var result = new Library.CalculateU(P: P, R: R).CalculateUByPAndR();

            Assert.That(result, Is.EqualTo(expected));
        }

    }
}